# Josh's Renovate Testing

This repository is for testing [Renovate](https://docs.renovatebot.com/).

The pipeline uses the [renovate-runner](https://gitlab.com/renovate-bot/renovate-runner)
templates.
